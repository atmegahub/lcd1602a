#pragma once

#ifndef LCD_LIB
#define LCD_LIB

//=========================================
// LCD
//=========================================

#ifndef LCD_DATA_PORT
#define LCD_DATA_PORT	PORTC
#endif

#ifndef LCD_DATA_DDR
#define LCD_DATA_DDR	DDRC
#endif

#ifndef LCD_SYNC_PORT
#define LCD_SYNC_PORT	PORTC
#endif

#ifndef LCD_SYNC_DDR
#define LCD_SYNC_DDR	DDRC
#endif

#ifndef LCD_D4_PIN
#define LCD_D4_PIN		PC0
#endif

#ifndef LCD_D5_PIN
#define LCD_D5_PIN		PC1
#endif

#ifndef LCD_D6_PIN
#define LCD_D6_PIN		PC2
#endif

#ifndef LCD_D7_PIN
#define LCD_D7_PIN		PC3
#endif

#ifndef LCD_E_PIN
#define LCD_E_PIN		PC4
#endif // LCD_E_PIN

# ifndef LCD_RS_PIN
#   define LCD_RS_PIN		PC5
# endif // LCD_RS_PIN

#define LCD_CLEAR		0x01

void lcd_init();
void lcd_com(unsigned char p);
void lcd_data(unsigned char p);
void lcd_string_stop();
void lcd_string(unsigned char command, char *string);

int lcd_string_stoped = 0;

#endif // LCD_LIB

