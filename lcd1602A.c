//*****************************************************************************

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "lcd1602A.h"

// init LCD
void lcd_init(void)
{
	// RS = 0 - command, 1 - data 
		
	_delay_ms(15);
		
	// pull up resistor
	LCD_SYNC_DDR |= (1 << LCD_E_PIN | 1 << LCD_RS_PIN); // 1 - out
	LCD_DATA_DDR |= (1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN); // 1 - out
	
	LCD_SYNC_PORT &= ~(1 << LCD_RS_PIN | 1 << LCD_E_PIN); // sync off
	LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN); // data off

	_delay_ms(5);
	
	// Конфигурирование четырехразрядного режима
	
	LCD_SYNC_PORT |= 1 << LCD_E_PIN; 
	LCD_DATA_PORT |= 1 << LCD_D5_PIN;   // D5 - 1 = Выбор числа линий, D4 = 0 - 4 бит
	LCD_DATA_PORT &= ~(1 << LCD_D4_PIN);	
	LCD_SYNC_PORT &= ~(1 << LCD_E_PIN);   // 0b00000100

	_delay_ms(15);
	
	lcd_com(0x28); // шина 4 бит, LCD - 2 строки
	lcd_com(0x08); // полное выключение дисплея
	lcd_com(0x01); // очистка дисплея
	lcd_com(0x06); // сдвиг курсора вправо
	lcd_com(0x0C); // включение дисплея, курсор не видим
}

void lcd_com(unsigned char p)
{
	LCD_SYNC_PORT &= ~(1 << LCD_RS_PIN); // RS = 0 (запись команд)
	LCD_SYNC_PORT |= (1 << LCD_E_PIN); // E = 1 (начало записи команды)
	LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off
	// LCD_DATA_PORT |= ((p & 0xF0) >> 4); // старший нибл
	
	// старший нибл
	LCD_DATA_PORT |= (((p & 0b00010000) >> 4) << LCD_D4_PIN);
	LCD_DATA_PORT |= (((p & 0b00100000) >> 5) << LCD_D5_PIN);
	LCD_DATA_PORT |= (((p & 0b01000000) >> 6) << LCD_D6_PIN);
	LCD_DATA_PORT |= (((p & 0b10000000) >> 7) << LCD_D7_PIN);
	
	LCD_SYNC_PORT &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	_delay_ms(2);
	
	LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off
	LCD_SYNC_PORT |= (1 << LCD_E_PIN); // E = 1 (начало записи команды)
	
	// LCD_DATA_PORT |= (p & 0x0F); // младший нибл	
	LCD_DATA_PORT |= (((p & 0b00000001)) << LCD_D4_PIN);
	LCD_DATA_PORT |= (((p & 0b00000010) >> 1) << LCD_D5_PIN);
	LCD_DATA_PORT |= (((p & 0b00000100) >> 2) << LCD_D6_PIN);
	LCD_DATA_PORT |= (((p & 0b00001000) >> 3) << LCD_D7_PIN);
	
	LCD_SYNC_PORT &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	
	_delay_ms(2);
	
	PORTC &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  //вылючить data
	_delay_ms(2);
}

// Функция передачи данных
void lcd_data(unsigned char p)
{
	 // старший нибл
	 LCD_SYNC_PORT |= (1 << LCD_RS_PIN) | (1 << LCD_E_PIN); // RS = 1 (запись данных), EN - 1 (начало записи команды в LCD)
	 LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off
	 
	 _delay_ms(1);
	 
	 // старший нибл
	 LCD_DATA_PORT |= (((p & 0b00010000) >> 4) << LCD_D4_PIN);
	 LCD_DATA_PORT |= (((p & 0b00100000) >> 5) << LCD_D5_PIN);
	 LCD_DATA_PORT |= (((p & 0b01000000) >> 6) << LCD_D6_PIN);
	 LCD_DATA_PORT |= (((p & 0b10000000) >> 7) << LCD_D7_PIN);
	 LCD_SYNC_PORT &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	 
	 _delay_ms(3);
	 
	 LCD_SYNC_PORT |= (1 << LCD_E_PIN); // EN = 1 (начало записи команды в LCD)
	 LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off

	 LCD_DATA_PORT |= (((p & 0b00000001)) << LCD_D4_PIN);
	 LCD_DATA_PORT |= (((p & 0b00000010) >> 1) << LCD_D5_PIN);
	 LCD_DATA_PORT |= (((p & 0b00000100) >> 2) << LCD_D6_PIN);
	 LCD_DATA_PORT |= (((p & 0b00001000) >> 3) << LCD_D7_PIN);

	 LCD_SYNC_PORT &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	 // LCD_SYNC_PORT &= ~(1 << LCD_RS_PIN); // RS = 0 (конец запись данных в LCD)
	 _delay_ms(3);
	 	 
	// старший нибл

	// LCD_SYNC_PORT |= (1 << LCD_RS_PIN) | (1 << LCD_E_PIN); // RS = 1 (запись данных), EN - 1 (начало записи команды в LCD)
	// LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off
	// LCD_DATA_PORT |= ((p & 0xF0) >> 4); // старший нибл
	// PORTC &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	// _delay_ms(1);
	// LCD_SYNC_PORT |= (1 << LCD_E_PIN); // EN = 1 (начало записи команды в LCD)
	// LCD_DATA_PORT &= ~(1 << LCD_D4_PIN | 1 << LCD_D5_PIN | 1 << LCD_D6_PIN | 1 << LCD_D7_PIN);  // data off
	// LCD_DATA_PORT |= (p & 0x0F); // младший нибл
	// LCD_SYNC_PORT &= ~(1 << LCD_E_PIN); // EN = 0 (конец записи команды в LCD)
	_delay_ms(1);
}

void lcd_string_stop()
{
	lcd_string_stoped = 1;
}

// Функция вывода строки на LCD
void lcd_string(unsigned char command, char *string)
{
	lcd_string_stoped = 0;
	lcd_com(0x0C);
	lcd_com(command);

	while(*string != '\0' && !lcd_string_stoped) 
	{
		lcd_data(*string);
		string++;
	}
	
	lcd_string_stoped = 0;
}