### Библиотека для работы с LCD 1602A

Библиотека работает с 2х строчнм дисплеем
использует линии данных LCD D4-D7

Пины синхронизации на одном порту  
Пины данных на одном порту  

```
#include "lcd1602A.c"

// config LCD

#define LCD_DATA_PORT	PORTC
#define LCD_DATA_DDR	DDRC

#define LCD_SYNC_PORT	PORTC
#define LCD_SYNC_DDR	DDRC

#define LCD_D4_PIN		PC0
#define LCD_D5_PIN		PC1
#define LCD_D6_PIN		PC2
#define LCD_D7_PIN		PC3

#define LCD_E_PIN		PC4
#define LCD_RS_PIN		PC5


int main(void)
{
    /* Replace with your application code */
	
	lcd_init();
	
	lcd_com(0x01); // clear LCD
	lcd_string(0x80 ,"Hello"); // запись на первой линии
	lcd_string(0xC0 ,"next line"); // запись на второй линии
	
    while (1) 
    {
	
    }
}
```